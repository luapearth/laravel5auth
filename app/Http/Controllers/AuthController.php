<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function index()
    {
        if (Auth::check()) return redirect()->to('/');
        return view('auth.index');
    }

    public function store(Request $request)
    {
        $cred = array(
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );
        if (Auth::attempt($cred)) {
            // Authentication passed...
            return redirect()->to('/');
        }
        else {
            return back()->with('error', 'Invalid username or password')->withInput();
        }
    }

    public function destroy()
    {
        Auth::logout();
        return redirect()->to('auth/login');
    }
}
