<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array('middleware' => 'auth', 'uses' => 'HomeController@index'));
//Route::get('/', 'HomeController@index');

Route::resource('/auth', 'AuthController', array('only' => array('index', 'store', 'destroy')));
Route::get('/auth/login', 'AuthController@index');
Route::post('/auth/login', 'AuthController@store');
Route::get('/auth/logout', 'AuthController@destroy');